require 'sinatra'
require 'json'
require 'httparty'
require_relative 'modify_request'

set :bind, '0.0.0.0'

def source_headers env
	h = {}
	env.each do |key, value|
		if key.start_with?("HTTP_") && key != "HTTP_HOST"
			h[key.split("HTTP_").last.gsub("_","-")] = value
		end
	end
	h
end

def source_request env
	request_uri = env['REQUEST_URI']
	url = "https://lidarr.media.home.msth.nl#{request_uri}"
	return url, source_headers(env)
end

def proxy_request request_method = :get
	url, h = source_request(request.env)
	puts "RAW", "-"*10,url, h
	mod_url = modify_url(url)
	mod_h = modify_headers(h)
	puts "MODIFIED", "-"*10,mod_url, mod_h

	if request_method == :post
		response = HTTParty.post(url, :headers => h)
	else
		response = HTTParty.get(url, :headers => h)
	end
end

get '/*' do 
	# puts JSON.pretty_generate(request.env)
	response = proxy_request
	response.body
end

post '/*' do
	response = proxy_request :post
	response.body
end

# request.env
# {
#   "rack.version": [
#     1,
#     3
#   ],
#   "rack.errors": "#<IO:0x000055936a95b8b8>",
#   "rack.multithread": true,
#   "rack.multiprocess": false,
#   "rack.run_once": false,
#   "SCRIPT_NAME": "",
#   "QUERY_STRING": "term=lidarr:7f001818-13a9-3eb8-a533-52c431a0feee",
#   "SERVER_PROTOCOL": "HTTP/1.1",
#   "SERVER_SOFTWARE": "puma 4.3.5 Mysterious Traveller",
#   "GATEWAY_INTERFACE": "CGI/1.2",
#   "REQUEST_METHOD": "GET",
#   "REQUEST_PATH": "/api/v1/album/lookup",
#   "REQUEST_URI": "/api/v1/album/lookup?term=lidarr:7f001818-13a9-3eb8-a533-52c431a0feee",
#   "HTTP_VERSION": "HTTP/1.1",
#   "HTTP_HOST": "localhost:4567",
#   "HTTP_USER_AGENT": "curl/7.54.0",
#   "HTTP_ACCEPT": "*/*",
#   "HTTP_X_API_KEY": "ac5f34c0c77942fca25be453b40fcac1",
#   "puma.request_body_wait": 0,
#   "SERVER_NAME": "localhost",
#   "SERVER_PORT": "4567",
#   "PATH_INFO": "/api/v1/album/lookup",
#   "REMOTE_ADDR": "172.17.0.1",
#   "puma.socket": "#<TCPSocket:0x000055936b1776a0>",
#   "rack.hijack?": true,
#   "rack.hijack": "#<Puma::Client:0x000055936b177678>",
#   "rack.input": "#<Puma::NullIO:0x000055936b9b69b0>",
#   "rack.url_scheme": "http",
#   "rack.after_reply": [],
#   "puma.config": "#<Puma::Configuration:0x000055936b180610>",
#   "sinatra.commonlogger": true,
#   "rack.logger": "#<Logger:0x000055936b176d40>",
#   "rack.request.query_string": "term=lidarr:7f001818-13a9-3eb8-a533-52c431a0feee",
#   "rack.request.query_hash": {
#     "term": "lidarr:7f001818-13a9-3eb8-a533-52c431a0feee"
#   },
#   "sinatra.route": "GET /*"
# }
