# Dynamic Proxy

This app is used to proxy and alter traffic.


## Example

```
2020/05/31 19:03:31 {"url":"/api/v1/album/lookup?term=lidarr:7f001818-13a9-3eb8-a533-52c431a0feee","method":"GET","headers":{"User-Agent":["Ombi"],"X-Api-Key":["ac5f34c0c77942fca25be453b40fcac1"]},"body":""}


2020/05/31 19:03:46 {"url":"/api/v1/album/lookup?term=lidarr:8fffd616-de95-3597-b1f2-211666cfc2dd","method":"GET","headers":{"User-Agent":["Ombi"],"X-Api-Key":["ac5f34c0c77942fca25be453b40fcac1"]},"body":""}
```
