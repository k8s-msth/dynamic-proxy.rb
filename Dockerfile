FROM ruby

WORKDIR /usr/src/app

ADD Gemfile Gemfile
RUN bundle install
ADD app.rb app.rb
ADD modify_request.rb modify_request.rb

ENTRYPOINT ["ruby", "app.rb"]
